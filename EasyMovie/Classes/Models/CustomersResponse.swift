//
//  CustomersResponse.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import Foundation

struct CustomersResponse: Codable {
    var success: Bool
    var customers: [Account]
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case customers = "customers"
    }

}
