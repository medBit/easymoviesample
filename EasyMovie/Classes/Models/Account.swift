//
//  Account.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import Foundation

struct Account: Codable {
    var id: String
    var name: String
    var logo: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case logo = "logo"
    }
}
