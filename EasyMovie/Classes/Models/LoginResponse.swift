//
//  LoginResponse.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import Foundation

/// A model of a login api response
class LoginResponse: Codable {
    var token: String
    var expiration: TimeInterval
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case expiration = "expires_in"
    }
}
