//
//  Constants.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import Foundation


enum Constants {
    
    enum AppConfig {
        static let projectConfig = "ProjectConfig"
        static let baseUrl = "APIBaseURL"
    }
    
    enum Keys {
        static let authKey = "com.med.easymovie.auth"
        static let username = "com.med.easymovie.username"
        static let password = "com.med.easymovie.password"
    }

}
