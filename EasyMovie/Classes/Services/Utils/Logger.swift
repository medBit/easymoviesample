//
//  Logger.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//


import Foundation

public enum LogLevel: Int {
    case none = 0
    case info = 1
    case warning = 2
    case error = 3
    case verbose = 4
}

public class Logger: NSObject {
    public static var logLevel: LogLevel = .verbose
    
    public static func info(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        if Logger.logLevel.rawValue < LogLevel.info.rawValue {
            return
        }
        
        print("[\((file as NSString).lastPathComponent) \(function) \(line)] [INFO] \(message)")
    }
    
    public static func warn(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        if Logger.logLevel.rawValue < LogLevel.warning.rawValue {
            return
        }
        
        print("[\((file as NSString).lastPathComponent) \(function) \(line)] [WARN] \(message)")
    }
    
    public static func error(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        if Logger.logLevel.rawValue < LogLevel.error.rawValue {
            return
        }
       
        print("[\((file as NSString).lastPathComponent) \(function) \(line)] [ERROR] \(message)")
    }
    
    public static func trace(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        if logLevel != .verbose {
            return
        }
        
        print("[\((file as NSString).lastPathComponent) \(function) \(line)] [TRACE] \(message)")
    }
}
