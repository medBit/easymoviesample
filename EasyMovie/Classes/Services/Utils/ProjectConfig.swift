//
//  ProjectConfig.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import UIKit

class ProjectConfig: NSObject {
    static var main: ProjectConfig = ProjectConfig()
  
    //api
    var apiBaseURL: String = ""
    
    override init() {
        super.init()
        
        
        let bundle = Bundle.main
        guard let config = bundle.object(forInfoDictionaryKey: Constants.AppConfig.projectConfig) as? [String: String] else {
            return
        }

        self.apiBaseURL = config.value(Constants.AppConfig.baseUrl)
    }
}

extension Dictionary where Key == String, Value == String {
    func value(_ key: String) -> String {
        guard let value = self[key] else {
            return ""
        }
        
        return value
    }
}
