//
//  CoreAPI.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//


import UIKit


enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

extension APIManager {
    static func request<T: Decodable>(endpoint: String,
                                      httpMethod: HTTPMethod,
                                      params: [String: String],
                                      completion: @escaping (Result<T, APIError>) -> Void) {
        
        // create the url
        let urlString = APIManager.api(endpoint: endpoint)
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidURL))
            return
        }

        // create the get request
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        // add content type header
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(APIManager.authorization)", forHTTPHeaderField: "Authorization")

        // create the http body
        request.httpBody = params.urlEncoded()

        // fire the request
        URLSession.shared.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(.failure(.apiError(error)))
                return
            }
            
            guard let data = data else {
                completion(.failure(.noData))
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let result = try decoder.decode(T.self, from: data)
                completion(.success(result))
            }
            catch {
                Logger.error(error.localizedDescription)
                completion(.failure(.decodingError))
            }
        }.resume()
    }
    
    static func get<T: Decodable>(endpoint: String,
                                  params: [String: String],
                                  completion: @escaping (Result<T, APIError>) -> Void) {
        
        APIManager.request(endpoint: endpoint, httpMethod: .get, params: params, completion: completion)
    }
    
    static func post<T: Decodable>(endpoint: String,
                                   params: [String: String],
                                   completion: @escaping (Result<T, APIError>) -> Void) {
        
        APIManager.request(endpoint: endpoint, httpMethod: .post, params: params, completion: completion)
    }
    
    static func loadAccounts(parameters: [String: String], completion: @escaping (Result<CustomersResponse, APIError>) -> Void) {
        let encodedParams = parameters.urlEncodedString()
        let endpoint = "secured/customers?\(encodedParams)"
        print(endpoint)
        APIManager.get(endpoint: endpoint, params: [:]) { (result: Result<CustomersResponse, APIError>) in
            completion(result)
        }
    }
}


enum APIManager {
    // MARK: Endpoints
    static let baseUrl: String = ProjectConfig.main.apiBaseURL
    
    static func api(endpoint: String) -> String {
        let url = APIManager.baseUrl.appending(endpoint)
        return url
    }
}

/// A high level representation of API errors
enum APIError: Error {
    case apiError(_ error: Error)
    case invalidResponse
    case ssoDisabled
    case invalidURL
    case noData
    case decodingError
    case encodingError
}

extension Dictionary {
    func urlEncoded() -> Data? {
        if self.keys.isEmpty {
            return nil
        }
        
        var components = URLComponents()
        components.queryItems = self.map { key, value in
            let item = URLQueryItem(name: "\(key)", value: "\(value)")
            return item
        }

        return components.query?.data(using: .utf8)
    }
    
    func urlEncodedString() -> String {
        if self.keys.isEmpty {
            return ""
        }
        
        var components = URLComponents()
        components.queryItems = self.map { key, value in
            let item = URLQueryItem(name: "\(key)", value: "\(value)")
            return item
        }

        return components.query ?? ""
    }
}
