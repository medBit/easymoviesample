//
//  CoreAPI.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//


import UIKit


enum AuthService {
    
    //access tokens
    static var authorization: String {
        get {
            guard let auth = UserDefaults.standard.string(forKey: Constants.Keys.authKey) else {
                return ""
            }
            
            return auth
        }
        set(newValue) {
            UserDefaults.standard.setValue(newValue, forKey: Constants.Keys.authKey)
        }
    }
    
    static var isSignedIn: Bool {
        let tokenExist = UserDefaults.standard.string(forKey: Constants.Keys.authKey) != nil
        return tokenExist
    }
    
    static var username: String? {
        get {
            let login = UserDefaults.standard.string(forKey: Constants.Keys.username)
            return login
        }
        set(newValue) {
            UserDefaults.standard.setValue(newValue, forKey: Constants.Keys.username)
        }
    }
    
    static var password: String? {
        get {
            let pass = UserDefaults.standard.string(forKey: Constants.Keys.password)
            return pass
        }
        set(newValue) {
            UserDefaults.standard.setValue(newValue, forKey: Constants.Keys.password)
        }
    }
}

extension AuthService {
    static func login(username: String,
                      password: String,
                      completion: @escaping (Result<String, APIError>) -> Void) {
        
        let params = ["email": username, "password": password]
        APIManager.post(endpoint: "login", params: params) { (result: Result<LoginResponse, APIError>) in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    completion(.success(response.token))
                }
                
                //save locally auth data
                AuthService.authorization = response.token
                AuthService.username = username
                AuthService.password = password
                
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
        
    }
    
    static func login(completion: @escaping (Result<String, APIError>) -> Void) {
        guard let login = username, let pass = password else {
            return completion(.failure(.ssoDisabled))
        }
        
        AuthService.login(username: login, password: pass, completion: completion)
    }
}


// Facade to the APIManager
extension APIManager {
    static var isSignedIn: Bool {
        let flag = AuthService.isSignedIn
        return flag
    }
    
    static var authorization: String {
        let token = AuthService.authorization
        return token
    }
}
