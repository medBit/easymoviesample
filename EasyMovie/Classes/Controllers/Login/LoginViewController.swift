//
//  LoginViewController.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, Storyboarded {
    weak var coordinator: MainCoordinator?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.emailTextField.text = AuthService.username
        self.passwordTextField.text = AuthService.password
        AuthService.login { [weak self] (result) in
            switch result {
            case .success:
                self?.coordinator?.showAccounts()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    
    @IBAction func loginButtonPressed() {
        guard let email = self.emailTextField.text, !email.isEmpty else {
            Logger.error("Email field empty")
            //TODO add alert
            return
        }
        
        guard let password = self.passwordTextField.text, !password.isEmpty else {
            Logger.error("Password field empty")
            //TODO add alert
            return
        }
        
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        AuthService.login(username: email, password: password) { [weak self] (result) in
            switch result {
            case .success:
                self?.coordinator?.showAccounts()
            case .failure(let error):
                print(error.localizedDescription)
                //TODO add alert
            }
        }
    }
}


extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
