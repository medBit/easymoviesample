//
//  MainCoordinator.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    var children = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = LoginViewController.instantiate()
        vc.coordinator = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    func showAccounts() {
        let vc = AccountsViewController.instantiate()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen

        vc.coordinator = self
        self.navigationController.present(nav, animated: true, completion: nil)
    }
}
