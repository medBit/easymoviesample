//
//  Storyboarded.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let className = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        guard let instance = storyboard.instantiateViewController(withIdentifier: className) as? Self else {
            fatalError("View Controller \(className) is not created in Main.storyboard")
        }
        
        return instance
    }
}
