//
//  AccountsViewController.swift
//  EasyMovie
//
//  Created by Med Hajlaoui on 01/04/2020.
//  Copyright © 2020 Med Hajlaoui. All rights reserved.
//

import UIKit

class AccountsViewController: UIViewController, Storyboarded {
    weak var coordinator: MainCoordinator?
    var customers: [Account] = [Account]()
    
    @IBOutlet weak var customersTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Accounts"
        
        //load accounts
        //TODO add pagination params in the data layer
        let params = ["start": "0", "limit": "30"]
        APIManager.loadAccounts(parameters: params) { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.customers = response.customers
                self?.customersTableView.reloadData()
            case .failure(let error):
                Logger.error(error.localizedDescription)
                //TODO add alert
            }
        }
        
    }
    
}
